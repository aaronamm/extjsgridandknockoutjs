﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExtJsGridAndKnockJs.Models;
using Newtonsoft.Json;

namespace ExtJsGridAndKnockJs
{

    /// <summary>
    /// Summary description for Customer
    /// </summary>
    public class CustomerHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            var httpMethod = context.Request.HttpMethod.ToUpper();
            switch (httpMethod)
            {
                case "GET":
                    GetCustomer(context);
                    break; 
                case "PUT":
                    UpdateCustomer(context);
                    break;
            }
            
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }


        public void GetCustomer(HttpContext context)
        {

            var customers = new 
                {
                    Customers = new List<Customer>()
                        {
                            new Customer()
                                {
                                    Id = 1,
                                    FirstName = "P' P",
                                    LastName = "PP",
                                    DateOfBirth  = new DateTime(1980,3,16)
                                },
                            new Customer()
                                {
                                    Id = 2,
                                    FirstName = "Amm",
                                    LastName = "Theeranit",
                                    DateOfBirth  = new DateTime(1985,3,16)
                                }
                        }
                };

            var jsonResult = JsonConvert.SerializeObject(customers, Formatting.Indented);
            context.Response.Write(jsonResult);

        }

        public void UpdateCustomer(HttpContext context)
        {
            var result = new
                {

                    Success = true
                };

            var jsonResult = JsonConvert.SerializeObject(result, Formatting.Indented);
            context.Response.Write(jsonResult);
        }
    }
}